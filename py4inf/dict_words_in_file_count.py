import string

fname = input('Enter file name: ')
try:
    fhand = open(fname)
except:
    print('File can not be opened: ', fname)
    exit()

counts = dict()

for line in fhand:
    line = line.translate([None, string.punctuation])
    line = line.lower()
    words = line.split()
    for word in words:
        counts[word] = counts.get(word,0) + 1
print(counts)

