#Exercise 10.2 This program counts the distribution of the hour of the day for each of the messages. You can pull the hour from the “From” line by finding the time string and then splitting that string into parts using the colon character. Once you have accumulated the counts for each hour, print out the counts, one per line,sorted by hour as shown below.
fhand = open('mbox-short.txt')

hour = dict()
lst = list()

for line in fhand:
    words = line.strip().split()
    if len(words) == 0 or words[0] != 'From' : continue
#    print(words[5])
    words = words[5].split(':')
#    print(words)
#    print(words[0])
    hour[words[0]] = hour.get(words[0],0) + 1
#print(hour)

for time, count in hour.items():
    lst.append((time , count))
lst.sort()
    
for key, val in lst:
    print(key, val)
