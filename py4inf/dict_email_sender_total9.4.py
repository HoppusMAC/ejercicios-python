#Exercise 3   Write a program to read through a mail log, build a histogram using a dictionary to count how many messages have come from each email address, and print the dictionary.
import string
 
fname = input('Enter file name: ')
try:
    fhand = open(fname)
except:
    print('Could not open file ', fname)

sender = dict()
largest = None

for line in fhand:
    line = line.translate([None, string.punctuation])
    words = line.strip().split()
    if len(words) == 0 or words[0] != 'From' : continue
#print data used    print(words[1])
    sender[words[1]] = sender.get(words[1],0) + 1

#Exercise 4   Add code to the above program to figure out who has the most messages in the file.
#After all the data has been read and the dictionary has been created, look through the dictionary using a maximum loop (see Section ??) to find who has the most messages and print how many messages the person has.
#
#Enter a file name: mbox-short.txt
#cwen@iupui.edu 5
#
#Enter a file name: mbox.txt
#zqian@umich.edu 195
    
    if sender.get(words[1],0) > sender.get(largest,0): 
        largest = words[1]
print(sender)
print(largest, sender.get(largest))
