fname = input('Enter a file name: ')
try:
	fhand = open(fname)
except:
	print('File can not be opened:', fname)
	exit()

count = 0 

for line in fhand:
	if line.startswith('Subject:'):
		count = count + 1

print('There are ', count, 'Subject: lines in ', fname)
