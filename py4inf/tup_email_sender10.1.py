#Read and parse the “From” lines and pull out the addresses from the line. Count the number of messages from each person using a dictionary.

fhand = open('mbox-short.txt')


sender = dict()

for line in fhand:
    words = line.strip().split()
    if len(words) == 0 or words[0] != 'From': continue
#    print(words[1])
    sender[words[1]] = sender.get(words[1],0) + 1
# print(sender)

#After all the data has been read, print the person with the most commits by creating a list of (count, email) tuples from the dictionary. Then sort the list in reverse order and print out the person who has the most commits.

lst = list()
for email , count in sender.items():
    lst.append((count , email))  
lst.sort(reverse=True)
print(lst[0][1], lst[0][0])
