fname = input('Enter a file name: ')
try:
	fhand = open(fname)
except:
	print('Invalid file name.')
	exit()

for line in fhand:
	print(line.rstrip().upper())