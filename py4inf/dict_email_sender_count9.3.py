#Exercise 3   Write a program to read through a mail log, build a histogram using a dictionary to count how many messages have come from each email address, and print the dictionary.
import string
 
fname = input('Enter file name: ')
try:
    fhand = open(fname)
except:
    print('Could not open file ', fname)

sender = dict()

for line in fhand:
    line = line.translate([None, string.punctuation])
    words = line.strip().split()
    if len(words) == 0 or words[0] != 'From' : continue
#print data used    print(words[1])
    sender[words[1]] = sender.get(words[1],0) + 1
print(sender)
