#Exercise 11.2 Write a program to look for lines of the form
#New Revision: 39772
#and extract the number from each of the lines using a regular expression and the
#findall() method. Compute the average of the numbers and print out the average.
import re

fname = input('Enter file: ')

fhand= open(fname)
total = 0
count = 0
i = 0

for line in fhand:
    line = line.strip()
    if re.findall('^New Revision:', line):
        val = re.findall('^New Revision: ([0-9]+)',line)

        total = total + int(val[0])
        count = count + 1
        print(val[0])
print(total/count)

