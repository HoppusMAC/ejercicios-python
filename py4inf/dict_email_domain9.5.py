#Exercise 5   This program records the domain name (instead of the address) where the message was sent from instead of who the mail came from (i.e., the whole email address). At the end of the program, print out the contents of your dictionary.

fname = input('Enter file name: ')

try:
    fhand = open(fname)
except:
    print('COULD NOT OPEN FILE ', fname)

domain = dict()

for line in fhand:
    words = line.strip().split()
    if len(words) == 0 or words[0] != 'From' : continue
    words = words[1].split('@')
    print(words)
    domain[words[1]] = domain.get(words[1],0) + 1
print(domain)

