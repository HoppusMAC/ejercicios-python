import urllib

fhand = urllib.urlopen('http://data.pr4e.org/romeo.txt')

counts = dict()

for line in fhand:
    line = line.decode().strip()
    print(line)
    words = line.split()
    for word in words:
        counts[word] = counts.get(word, 0 ) + 1
print(counts)


